# Addons Coca-Cola Talca

Addons de SAP Business One para el control de proceso de manofactura de etiquetas y tapas del proceso de embotellamiento de productos de la fábrica de Coca-Cola en Talca.

![](https://img.shields.io/badge/versi%C3%B3n-v.1.0-red.svg) ![](https://img.shields.io/badge/estado-finalizado-green.svg)

## Cliente

Coca-Cola Talca

## Softwares

| Nombre                   | Versión |
| ------------------------ | ------- |
| Visual Basic Studio 2015 | 14.0    |
| SAP Business One         | 9.0     |
| Hana Studio              | 2.3.5   |



## Control de Cambios

| Versión | Descripción                                             | Fecha            |
| ------- | ------------------------------------------------------- | ---------------- |
| x       | Añadidas las funciones de etiquetado                    | 25 de Marzo 2019 |
| x       | Añadidas las funciones de tapas y mejoras en etiquetado | 15 de Abril 2019 |
| x       | Realización de pruebas                                  | 20 de Abril 2019 |
| 1.0     | Solución finalizada                                     | 2 de Junio 2019  |

## Contactos

| Nombre         | Email                  | Teléfono                         |
| -------------- | ---------------------- | -------------------------------- |
| Jaime Gonzales | jmgonzales@cocacola.cl | 56 44 1294 9348                  |
| Andrés Mellado | amellado2@cocacola.cl  | 56 44 8567 8757 - 56 9 6581 8623 |

## Integrantes

| Nombre         | Cargo         | Email              |
| -------------- | ------------- | ------------------ |
| Adán Mendoza   | Jefe Proyecto | amendoza@seidor.cl |
| Jaime Araya    | esarrollador  | ajimenez@seidor.cl |
| Óscar Bernales | Desarrollador | sabarca@seidor.cl  |

