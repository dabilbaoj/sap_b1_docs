# 1. Introducción

El siguiente documento tiene por objetivo definir lineamientos generales para los desarrollos realizados en SQL para mejorar la mantención, legibilidad y comprensión del mismo al interior del equipo de trabajo. Este documento fue desarrollado para el equipo de desarrollo del área Business One de Seidor Chile.

## 1.1 Alcance

Este documento no avala la independencia al momento de desarrollar. Hay que considerar las opiniones del grupo de trabajo para implementar determinadas nomenclaturas o formatos que sean conocidos por todos.

## 1.2 Terminología y Definiciones

En el documento hace referencia a la siguiente terminología y definiciones:

- **Camel Case**: Determina que la primera letra de cada palabra debe comenzar con minúscula y las siguientes con mayúscula. **Ejemplo**: `camelCase`.
- **Pascal Case**: Determina que todas las letras de las palabras que conformen un nombre deben comenzar con mayúscula. **Ejemplo**: `PascalCase`.

## 1.3 Resumen

Esta sección contiene tablas que describen un resumen  de las principales normas cubiertas en este documento. Las tablas contienen un resumen que ofrecen una mirada rápida a los puntos que se tocarán.

### 1.3.1 Convención de Nombres

| Identificador              | Prefijo | Formato | Notas                                                     |
| -------------------------- | ------- | ------- | --------------------------------------------------------- |
| Base de datos usuario      | x       | M       |                                                           |
| Tablas de usuario          | @SEI_   | M_      | Deben ser plurales y las palabras estar separadas por "_" |
| Campos de usuario          | U_      | P       | Deben ser escritos en singular.                           |
| Procedimientos almacenados | sp      | P       |                                                           |
| Funciones                  | sp      | P       |                                                           |
| Vistas                     | sp      | P       |                                                           |

> "M" = Mayúscula.      "P" = Pascal Case.      "_" = Prefijo.      "x" = No aplica.

### 1.3.2 Estilo Código

| Código      | Estilo                                                       |
| ----------- | ------------------------------------------------------------ |
| Indentación | El código debe quedar lo más legible posible. Dejar una variable o sentencia por línea. |
| Comentarios | Solo utilizar `--` para comentar líneas de código y `/**/` en caso de comentar extractos de código muy largos. |

------

# 2. Convención de Nombres

La clave para hacer mantenible el código, es seguir estas convenciones. Los siguientes lineamientos son una guía para nombrar sus tablas, campos y procedimientos almacenados.

## 2.1 Lineamientos Generales

1. Utilizar convención señalada dependiendo del datos.
2. Evitar abreviaciones para palabras mayores a 5 caracteres. **Ejemplo**: `MTES(MATERIALES)`.
3. Entidades que se representen a través de siglas deben estar escritas de similar manera. Ejemplo: `SII`, `ENAP`, etc.
4. No utilizar palabras reservados por SQL, HANA y SAP B1.
5. En caso de realizar abreviaciones, estas deben estar documentadas.
6. Los nombres de tablas deben estar escritos en plural.
7. Los nombres de los campos deben estar escritos en singular.
8. Los nombres de tablas y campos deben ser lo más representativo posible. **Ejemplo**: `@SEI_LISTA_MTES_AGRA NO @SEI_LISTA_MTES_1`
9. Los nombres no deben estar compuestos de más de 4 palabras. Estas deben representar en su totalidad a la entidad.
10. Evitar el uso de preposiciones.
11. Evitar numerar las tablas o campos. **Ejemplo**: `U_ApellidoPaterno NO U_Apellido1`.
12. Utilizar el idioma que prefiera el cliente.
13. Evitar el uso de caracteres propios del lenguaje español como la letra "**Ñ**" y **acentos**.
14. Los nombres de los campos creados para tablas de usuario no deben coincidir con los campos de tablas de SAP B1.



## 2.2 Uso de nombre y sintaxis

| Identificador              | Convención de nombres                                        |
| :------------------------- | ------------------------------------------------------------ |
| Tablas de usuario          | `@SEI_MAYUSCULA` Deben estar escritos en plural.. **Ejemplo**:`@SEI_LISTA_MTES` |
| Campos de usuario          | `U_PascalCase` Deben estar escritos en singular. **Ejemplo**:`U_NombItem` |
| Procedimientos almacenados | `spPascalCase_PascaCase` **Ejemplo**: `spProd_ObtenerProd`   |

------

# 3. Estilo de Código

La escritura de código inconsistente provoca una mayor incoherencia y problemas entre los desarrolladores. Es normal que al interior de un equipo de desarrollo cada uno de los integrantes tenga un estilo propio. Sin embargo, el diseño, el formato y la organización son la clave para crear un código que se pueda mantener.
Las siguientes secciones describen una forma de implementar código en SQL, para crear un código legible, ordenado y consistente que sea fácil de entender y mantener.

## 3.1 Formato

1. Sentencias como `FROM`, `WHERE`, `SELECT`, etc. deben estar siempre escritas en mayúscula.
2. No utilizar `SELECT *` . Solo solicitar los campos requeridos.
3. Usar paréntesis para mejorar la legibilidad.
4. Para procedimientos almacenados utilizar `sp`. Nunca `sp_` , ya que esta reservado por SQL.
5. Usar espacios en blanco para separar el código. Ejemplo:
6. Uso de comillas simples para delimitar strings.
7. En caso de trabajar en una base productiva, toda sentencia como `UPDATE`, `DELETE` u otras que modifiquen la base de datos, **ESTÁ TOTALMENTE PROHIBIDA**.
8. No utilizar espacios en los nombres de las tablas. Deben estar separadas por “`_`”.

## 3.2 Comentarios

1. Utilizar comentarios mediante “`--`” para comentar líneas de código.
```sql
    SELECT *
    FROM @SEI_LISTA_MTES, OITM
    WHERE @SEI_LISTA_MTES.NombItem = OITM.ItemName --Comparacion solo con nombres
```

2. Comentarios para trozos de código muy extensos, utilizar `/**/`.

```sql
    /*
    * Se necesita obtener todos los datos.
    */
    SELECT *
    FROM @SEI_LISTA_MTES, OITM
    WHERE @SEI_LISTA_MTES.NombItem = OITM.ItemName
```

3. Queries o procedimientos almacenados deben llevar siempre al inicio el siguiente formulario.

```sql
    /*
    * Nombre: ProgramaUno
    * Descripción: El programa uno hace tal cosa
    *Listado Funciones
    *-----FuncionUno.
    *-----FuncionDos.
    * Ultimo cambio: 27 de febrero
    * Autor(es): Seidor 1, Seidor 2
    * Cliente: AAABBB
    * Departamento Business One - Seidor Chile
    */
```