[TOC]

# 1. Introducción

El siguiente documento tiene por objetivo definir lineamientos generales para los desarrollos realizados en el lenguaje de programación C# para mejorar la mantención, legibilidad y comprensión del mismo al interior del equipo de trabajo. Este documento fue desarrollado para el equipo de desarrollo del área Business One de Seidor Chile.

## 1.1 Alcance

Este documento no avala la independencia al momento de desarrollar. Hay que considerar las opiniones del grupo de trabajo para implementar determinadas nomenclaturas que sean conocidas por todos.

## 1.2 Terminología y Definiciones

En el documento hace referencia a la siguiente terminología y definiciones:

- **Camel Case**: Determina que la primera letra de cada palabra debe comenzar con minúscula y las siguientes con mayúscula. **Ejemplo**: `camelCase`.
- **Pascal Case**: Determina que todas las letras de las palabras que conformen un nombre deben comenzar con mayúscula. **Ejemplo**: `PascalCase`.

## 1.3 Resumen

Esta sección contiene tablas que describen un resumen  de las principales normas cubiertas en este documento. Las tablas contienen un resumen que ofrecen una mirada rápida a los puntos que se tocarán.

### 1.3.1 Convención de Nombres

| Identificador      | Público | Privado | Notas                                  |
| ------------------ | ------- | ------- | -------------------------------------- |
| Archivo Proyecto   | P       | x       | Coincidir con el proyecto.             |
| Archivo Fuente     | P       | x       | Coincidir con la clase contenida.      |
| Otros Archivos     | P       | x       | Aplicar en lo posible.                 |
| Namespace          | P       | x       | Coincidir parcialmente con el archivo. |
| Clase o Estructura | P       | P       | Añadir a las subclases como sufijo.    |
| Método             | P       | P       | Usar verbos.                           |
| Propiedad          | P       | P       | No usar prefijos como `Get` o `Set`.   |
| Constante          | P       | _c      |                                        |
| Evento             | P       | P       |                                        |
| Parámetro          | _c      | _c      |                                        |
| Variable           | P       | P       | Evitar caracteres únicos o numéricos.  |
| Variable Auxiliar  | P       | P       | Evitar caracteres únicos o numéricos.  |
| Interfaces         | P       | P       |                                        |

> "c" = Camel Case.      "P" = Pascal Case.      "_" = Prefijo.      "x" = No aplica.

### 1.3.2 Estilo Código

| Código           | Estilo                                         |
| ---------------- | ---------------------------------------------- |
| Archivos Fuentes | Una clase por archivo.                         |
| Llaves `{}`      | Usar con salto de línea.                       |
| Indentación      | El código debe quedar espaciado de las llaves. |
| Comentarios      | Usar `//` ó `///` pero no `/* */`.             |
| Variables        | Una variable por declaración.                  |

### 1.3.3 Uso

| Código        | Estilo                                                    |
| ------------- | --------------------------------------------------------- |
| Propiedades   | Nunca usar prefijos como `Get` o `Set`.                   |
| Métodos       | Usar un máximo de 7 parámetros.                           |
| Condicionales |                                                           |
| Eventos       | Comprobar si hay `null` antes de llamar al evento.        |
| Excepciones   | Utilizar `trow`, `try` y `catch` siempre y cuando sea necesario. |



# 2. Convención de Nombres

La clave para hacer mantenible el código, es seguir estas convenciones. Los siguientes liniamientos son una guía para nombrar sus proyectos, archivos fuente, variables, propiedades, métodos, parámetros, clases, entre otros.

## 2.1 Lineamientos Generales

1. Siempre utilizar Camel Case o Pascal Case para nombres.
2. Evitar MAYÚSCULAS y minúsculas para nombres. Solo se permite para letras.
3. No crear declaraciones del mismo tipo y nombre.
4. No utilizar nombres que comiencen con números.
5. Elegir nombres concretos que especifiquen la intención.
6. Las variables deben describir a la entidad no su tipo o tamaño.
7. Evitar este tipo de notaciones (`strPalabra`, `iContador`)
8. Evitar la abreviación de nombres salvo que sea necesario.
9. Evitar la abreviación de nombres que tengan menos de 7 caracteres.
10. Toda abreviación debe ser documentada o señalada al inicio del código.
11. Usar mayúsculas para siglas y PascalCase para las demás. **Ejemplo**: `SII`.
12. No utilizar palabras reservadas del lenguaje C#.
13. Evitar nombres que entren en conflicto con el entorno de desarrollo.
14. Evitar añadir prefijos o sufijos redundantes. **Ejemplo**: `struct CuadradoStruct`, `class CHumano`.
15. Usar prefijos como ”Tiene”, “Es”, “Hay” para variables "booleanas".
16. Las clases deben llevar por nombre un sustantivo y singular, salvo que represente alguna multiplicidad.
17. Al escribir es necesario que se consideren a lo más 4 palabras que representen a la entidad.
18. Evitar el uso de preposiciones en exceso.
19. Usar el prefijo I para interfaces. **Ejemplo**: `IEntidad`.
20. Utilizar el nombre de la compañia o proyecto en el `namespace`. **Ejemplo**: `Winpack.Utilidades`.
21. Evitar incluir el nombre de la clase en la propiedad. **Ejemplo:** `Customer.Name` NO `Customer.CustomerName`.



## 2.2 Uso de nombre y sintaxis

| Identificador      | Convención de nombres |
| :----------------- | --------------------- |
| Archivo Proyecto   | `PascalCase` Debe coincidir con el nombre del proyecto. **Ejemplo**:`EuroPlantProduccion.csproj` |
| Archivo Fuente     | `PascalCase` Debe coincidir con el nombre de la clase que posee. Describir de modo general su funcionalidad. **Ejemplo**:`ListaMateriales.cs` |
| Otros Archivos     | `PascalCase` **Ejemplo**: `MaterialesNecesarios.xml`|
| Namespace          | `PascalCase` Debe coincidir con el nombre de la clase. **Ejemplo**: `ListaMaterial`|
| Clase o Estructura | `PascalCase` Debe representar de forma general la funcionalidad de la clase. **Ejemplo**: `ClaseMaterialProduccion`|
| Método             | `PascalCase` Debe describir de forma breve la funcionalidad del método. **Ejemplo**: `ImprimirListadoMateriales`|
| Constante          | `PascalCase` Deben ser específicas. **Ejemplo**: `const int VelocidadLuz = 300;`|
| Evento             | `PascalCase` Deben ser específicos y no coincidir con los que provee el entorno de programación. **Ejemplo**: `TriplePulsadoClickMouse`|
| Parámetro          | `_camelCase` Los parámetros de un método deben ser claros y específicos, además de especificar su propiedad. **Ejemplo**: `_nombreParametro`|
| Variable           | `PascalCase` Deben representar al objeto por completo. **Ejemplo**: `NombreMaterial`|
| Variable Auxiliar  | En caso de ser necesarias. Para números se asignan las letras “i,j,k,l” y para caracteres o cadenas de texto “c,d,e,f” **Ejemplo**: `return c;`, `for (int i = 0; i < VelocidadLuz; i++){}`|
| Interfaces         | `IPascalCase` Deben representar al objeto por completo. **Ejemplo**: `ICantidadPlatanos`|



# 3. Estilo de Código

La escritura de código inconsistente provoca una mayor incoherencia y problemas entre los desarrolladores. Es normal que al interior de un equipo de desarrollo cada uno de los integrantes tenga un estilo propio. Sin embargo, el diseño, el formato y la organización son la clave para crear un código que se pueda mantener.
Las siguientes secciones describen una forma de implementar código con el lenguaje de programación C# para crear un código legible, ordenado y consistente que sea fácil de entender y mantener.

## 3.1 Formato

1. Nunca declarar más de un namespace por archivo.
2. No poner más de una clase por archivo.
3. Las llaves deben ir en una nueva línea.
4. Siempre utilizar llaves en una declaración condicional.
5. El código debe estar indentado, ya sea de manera automática o tabulación.
6. Declarar cada variables independientemente.
7. Las clases deben estar implementadas de la siguiente forma:
   a. Variables
   b. Constructores
   c. Estructuras y Sub-clases
   d. Propiedades
   e. Métodos
8. La declaración por tipos debe hacer la siguiente secuencia:
   a. Public
   b. Protected
   c. Internal
   d. Private
9. Todo el código debe ser escrito en el mismo idioma.
10. Resumir y comentar a través de “summary” cada función o método realizado.
11. Detallar, de ser necesario, de donde provienen datos externos.
12. Al inicio de cada archivo detallar brevemente en qué consiste y sus componentes.
13. La extensión de línea de código no debe superar los 120 caracteres. En caso de superar aplicar salto de línea en la coma más cercana.
14. Debe existir un espaciado posterior a las comas. **Ejemplo**: `(a, b, c)`
15. Debe existir un espaciado al momento de realizar operaciones lógico-matemáticas. **Ejemplo**: `a = 1 + 2`
16. Deben existir saltos de línea de forma tal que el código quede encapsulado intentando mejorar la legibilidad.
17. Los archivos no deben exceder la cantidad de 2000 líneas de código.



## 3.2 Comentarios

18. Todos los comentarios deben ser escritos en el mismo idioma.
19. Utilizar `//` o `///` para comentar código.
20. No hacer flowerbox. **Ejemplo**:
```csharp
      // *************************************** 
      // Comment block
      // ***************************************
```
21. Utilizar comentarios en línea siempre y cuando se necesite especificar con más detalle.
22. En caso de realizar modificaciones en código. Tener en consideración lo siguiente:
```csharp
      //POR HACER: Especificar qué código se hará.
      //MODIFICADO: Especificar qué línea o función fue modificada.
      //MEJORA: En caso de modificar el código para mejorarlo.
      //MOMENTANEO: Parte del código no está completo. O en caso de comentar lineas para ejecutar sin problemas.    
```
23. Al inicio de cada archivo relacionado con el proyecto  se debe completar el siguiente formato:
```csharp
      /*
      * Nombre: ProgramaUno
      * Descripción: El programa uno hace tal cosa
      *Listado Funciones
      *-----FuncionUno.
      *-----FuncionDos.
      * Ultimo cambio: 27 de febrero
      * Autor(es): Seidor 1, Seidor 2
      *Cliente: AAABBB
      *Departamento Business One - Seidor Chile
      */
```
24. Para comentar métodos o funciones utilizar `summary`:
```csharp
      /*
      */
```



# 4. Uso del Lenguaje

## 4.1 Variables

1. Inicializar las variables donde fueron declaradas.
2. Elegir siempre el tipo de dato mas simple. Siempre y cuando se pueda.
3. Siempre declarar las variables con private, public, protected, etc. en caso que sea necesario.
4. Utilizar siempre int para valores pequeños y que no sean fracciones.
5. Utilizar long para valores numéricos que excedan la capacidad de int.
6. Usar double para numeros relacionados con operaciones fraccionarias para mejorar la exactitud del resultado.
7. Utilizar decimal para cálculos que necesiten aproximaciones.
8. Nunca concatenar strings al interior de un loop.
9. No comparar strings con String.Empty o "". Utilice String.Length == 0.
10. Preferir el uso de tipos de datos del lenguaje C# por sobre los creados personalmente. En caso contrario documente librerías externas.



## 4.2 Control

1. Evitar crear métodos recursivos. Utilice siempre "loops".

2. Evitar utilizar `foreach` sobre objetos inmutables.

3. No modificar datos utilizando `foreach`.

4. No evaluar un dato Booleano con true o false.  **Ejemplo**:

```csharp
	// MALO!
	if (esVerdad == true)
	{…}
	// BUENO!
	if (esVerdad)
	{…} 

```

5. Evitar el uso de expresiones condicionales compuestas. **Ejemplo**:

```csharp
	// MALO!
	if (((value > _highScore) && (value != _highScore)) && (value < _maxScore))
	{…}
	// BUENO!
	isHighScore = (value >= _highScore);
	isTiedHigh = (value == _highScore);
	isValid = (value < _maxValue);
	if ((isHighScore && ! isTiedHigh) && isValid)
	{…} 
```

6. Preferir el uso de  `if/else` en lugar de `switch/case` para procesos condicionales cortos.



## 4.2 Excepciones

1. Utilizar `catch` en excepciones que se puedan manejar.
2. Nunca declarar `catch` vacíos en los proyectos, salvo en pruebas o debug de código.
3. Evitar el anidamiento de `try/catch`.



## 4.3 Objetos

1. Declarar siempre de manera explicita los tipos al interior de un `namespace`.
2. Evitar el uso excesivo de `public` en las variables de una clase.
3. Evitar declarar métodos con mas de 5 parámetros. Considerar re-hacer el código.
4. 



# 5. Referencias

- https://docs.microsoft.com/es-es/dotnet/csharp/programming-guide/inside-a-program/coding-conventions
- https://www.danysoft.com/free/csharp2.pdf
- https://ymatias.com/2016/07/27/estandares-de-codificacion-y-nomenclatura-c/
- http://epicalsoft.blogspot.com/2015/01/c-estandares-de-codificacion-y.html
- https://programacion.net/articulo/top_9_buenas_practicas_para_desarrolladores_de_codigo_1349
- http://dis.um.es/~bmoros/privado/bibliografia/LibroCsharp.pdf
- http://programacion.jias.es/2017/09/estandares-de-nomenclatura-snake-case-kebab-case-camel-case/
- http://codigolinea.com/2008/05/25/estilo-de-programacion-y-convencion-de-nombres-ii/

