# 1. Introducción

El siguiente documento tiene por objetivo dar a conocer los lineamientos, reglas y guia de uso para utilizar las herramientas de control de versiones que ocupa el equipo de trabajo de SAP Business One de la empresa Seidor Chile.

# 2. Reglas

1. Todo cambio en el código o proyecto debe estar acompañado de un `commit`.
2. La rama master nunca debe ser modificada de forma personal.
3. Para realizar contribuciones al código se deben crear ramas con el nombre de la tarea. Estas ramas deben ser creadas a partir de la rama `develop` que este disponible.
4. La notación para nuevas ramas, excluyendo la rama `master` y `develop`, debe estar escrita en minúsculas y reemplazar los espacios por guiones bajos "_".  **Ejemplo**:  `generar_codigo_qr`
5. Se debe realizar `merge` a la rama `develop` y su posterior `push` al repositorio. Una vez la tarea asignada este completa.
6. Se debe realizar `push` a las ramas tarea de forma diaria. Solo en el caso de que se hayan realizado modificaciones.
7. En caso de que el código fuese realizado en conjunto, se debe incluir a los co-autores.
8. Finalizado y probado el desarrollo, se debe realizar el `merge` de `develop` en la rama master. Al momento de realizarlo se debe modificar la versión de la rama master.
9. Las pruebas de código deben realizarse en conjunto con el equipo de desarrollo en caso de que surjan dudas o propuestas por otros integrantes.
10. Realizar `push` a la rama `master` cuando las pruebas en la rama `develop` sean un exitosas.
11. Nunca borrar las ramas durante el proceso de desarrollo. Salvo por equivocaciones u otro tipo de contingencia.
12. Los repositorios creados para desarrollos de productos o soluciones a clientes deben ser creados como privados.
13. Solo los participantes del desarrollo pueden tener acceso al repositorio privado. 
14. El nombre del repositorio para un desarrollo debe llevar el nombre del cliente y el nombre de la solución. **Ejemplo**: `winpack_impresion_etiquetas`.
15. Los repositorios públicos solo alojarán documentación del equipo SAP Business One. **Ejemplo**: Lineamientos C#.
16. Toda imagen o archivos relacionados con un proyecto debe estar alojada en el repositorio en una carpeta de "assets" en caso que el proyecto no lo permita.
17. La documentación del proyecto debe estar alojada en una carpeta "`docs`" en la raíz de la carpeta del proyecto. **Ejemplo**: Documentación de requerimientos.
18. Los repositorios deben ser clonados directamente del servidor **Bitbucket**. Toda transferencia de archivos mediante otro medio (USB, correo electrónico, etc.) **ESTA TOTALMENTE PROHIBIDA**.
19. La creación del repositorio asociado a la creación de un proyecto o solución debe ser realizada por el encargado del proyecto. En caso de no estar disponible, debe ser realizada por un responsable designado.
20. Antes de realizar cualquier proyecto se debe crear un archivo LEEME o README. En este archivo se llevará el control de versión del proyecto, así como también integrantes y listado de tareas para el mismo.
21. En caso de no contar con el equipo otorgado por la empresa Seidor Chile. Los permisos deben ser asignados por el encargado del proyecto.
22. Toda rama del proyecto debe ser clonada en el equipo y hacer los `push` o `comits` pertinentes hacia el servidor de **Bitbucket**. No se deben hacer cambios en el repositorio vía web.
23. El cliente nunca tendrá acceso al repositorio. La entrega de soluciones o proyectos es a través de instalador u otro.
24. Los usuarios deben crear su cuenta con el email proporcionado por la empresa Seidor.
25. Todos los derechos del desarrollo deben estar adjudicados al equipo Business One o simplemente a la empresa Seidor Chile.
26. En el archivo README se deben incluir las versiones de programas u otros. El equipo de desarrollo debe trabajar en el mismo entorno y versiones de software.
27. Los repositorios deben ser clonados en su defecto en la máquina virtual del cliente o en el equipo otorgado por Seidor Chile.
28. Finalizado el proyecto los repositorios clonados deben ser eliminados tanto de la máquina virtual del cliente o equipo de desarrollo.
29. Finalizado el proyecto se quitarán los privilegios a los desarrolladores. Solo el jefe de proyecto tendrá acceso.
30. Nunca dejar los repositorios clonados en el Escritorio de la máquina virtual.



# 3. Uso de Bitbucket

Existen varias formas de utilizar Bitbucket (o git en su defecto) para la creación y edición de código. Estas formas pueden ser a través de comandos de consola o utilizar clientes gráficos. En este documento se propone el uso de [SourceTree](https://www.sourcetreeapp.com/) (softwarede Atlassian) como herramienta para el control de versiones tanto para el manejo de Git y Mercurial.



## 3.1 Guía de uso de Bitbucket

A continuación se señalan los pasos para el uso de **Bitbucket** y la herramienta **SourceTree**.



-- Preguntar que herramienta quieren ocupar.



# 4. Commits

## 4.1 Estructura

Para mejorar la legibilidad y entendimiento de un commit en Git, es necesario establecer ciertas reglas y estandares para todo el equipo de trabajo. A continuación de detalla la estructura del commit y como debe ser escrito.

```
Type: Subject

Body

Footer
```

### 4.1.1 Type

Para llevar un mejor control y tener un orden. Es necesario definir las siguientes nomenclaturas para tener una categorización de los tipos de cambios que pueden surgir durante el desarrollo.

| Type           | Descripción                                                  |
| -------------- | ------------------------------------------------------------ |
| **`feat`**     | Se añade una nueva caracterisca o funcionalidad al código.   |
| **`fix`**      | Se soluciona un bug o reparación al código.                  |
| **`docs`**     | Se realizó un cambio a la documentación.                     |
| **`style`**    | Se aplicó formato faltante al codigo. Se idento de otra forma, se añadieron espacios, etc. |
| **`refactor`** | Refactorización del código en producción.                    |
| **`test`**     | Se probó el código. No se realizaron cambios en el código.   |
| **`chore`**    | Actualización de tareas de build, configuración del admin. de paquetes; Sin cambios en el código. |

### 4.1.2 Subject

El Subject va situado en la misma línea del Tipo. El asunto no debe contener más de 50 caracteres, debe detallar en lo posible que se realizó durante la modificación. Piense en el Subject como el “Asunto” cuando escribe un correo electrónico.

### 4.1.3 Body

Al momento de escribir el Body es necesario detallar más en profundidad, en caso que se necesite, algunas especificaciones o detalles que describan lo que fue realizado. Debe ceñirse a responder el ¿Qué? y ¿Porque?.

### 4.1.4 Footer

Este elemento es opcional. Pero puede ser utilizado para hacer el seguimiento de los IDs o incidencias en el código.

## 4.2 Ejemplo

```
fix: Reparacion de funciones en clase vehiculos.

Se repararon las siguientes funciones:
-AnadirVehiculo
-EliminarVehiculo
-ActualizarVehiculo
Las funciones anteriores no modificaban la base de dato.

Resolves: #548

```



# 5. README

Para tener un control más eficiente de código de un determinado proyecto es necesario tener en la carpeta raíz del proyecto un archivo REAME(LEEME) para abordar los aspectos generales del código, ya sea para tener en consideración datos o información del cliente, control de cambios del proyecto, equipo de desarrollo u otros.

## 5.1 Formulario

Los campos o información que deben ser completadas, son los siguientes:

- **Nombre del Proyecto**
- **Versión Actual**: Establecer la versión actual o final del proyecto.
- **Descripción del Proyecto**: Describir brevemente los aspectos generales del proyecto.
- **Cliente**
- **Software**: Incluir el nombre del software o aplicativos de desarrollo y versión.
- **Control de cambios en el código**: Esto lo hace git en su defecto, pero llevar un listado de cambios relevantes dará mas orden y mejor legibilidad para el equipo de desarrollo.
- **Contactos**: Se debe completar los datos como nombre, correo electronico o telefono de aquellos contactos o cliente con quienes se trabaja.
- **Fecha de solicitud**
- **Fecha de entrega o posible fecha de entrega**
- **Responsable del desarrollo**
- **Equipo de desarrollo**: Completar los datos de todos los miembros del equipo de trabajo. Esto permitirá, en caso de no tener medios de contacto, acceder de forma más rápida a estos datos. Se debe incluir al encargado o responsable del desarrollo.



> **NOTA**: Para cada repositorio creado se incluirá una plantilla para completar los campos anteriormente señalados.



# 6. Referencias

+ https://confluence.atlassian.com/bitbucket/copy-your-repository-and-add-files-729980492.html
+ 